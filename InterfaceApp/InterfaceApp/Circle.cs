﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceApp
{
    class Circle : ICircle
    {
        public Circle(double r)
        {
            R = r;
        }
        public Circle()
        { }

        public double R { get; set; }


        public double GetArea()
        {
            return Math.PI * R * R;
        }

    }
}
