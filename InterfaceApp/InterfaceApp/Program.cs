﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceApp
{
    class Program
    {
        private static List<IFigure> figure = new List<IFigure>(); 

        static void Main(string[] args)
        {
            double r;
            double b, h;
            var circle = new Circle();
            var triangle = new Triangle();  

            for(int i=0; i<4;i++)
            {
                Console.WriteLine($"Triangle nr.{i + 1}");
                Console.Write("Type b: ");
                b = Convert.ToDouble(Console.ReadLine());
                Console.Write("Type h: ");
                h = Convert.ToDouble(Console.ReadLine());
                triangle = new Triangle(b, h);
                figure.Add(triangle);
            }
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine($"Circle nr. {i + 1}");
                Console.Write("Type r: ");
                r = Convert.ToDouble(Console.ReadLine());
                circle = new Circle(r);
                figure.Add(circle);
            }

            foreach(var item in figure)
            {
                Console.WriteLine(item.GetArea());
            }

            Console.WriteLine($"Sum: {figure.GetAreas()}");
            Console.ReadKey();
        }
    }
}
