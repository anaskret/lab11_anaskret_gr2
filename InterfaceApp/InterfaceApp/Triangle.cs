﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceApp
{
    class Triangle : ITriangle
    {
        public Triangle(double b, double h)
        {
            B = b;
            H = h;
        }
        public Triangle()
        { }
        public double B { get; set; }
        public double H { get; set; }

        public double GetArea()
        {
            return 0.5 * B * H;
        }

    }
}
