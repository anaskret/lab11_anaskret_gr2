﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceApp
{
    public interface ITriangle : IFigure
    {
        double B { get; set; }
        double H { get; set; }
    }
}
