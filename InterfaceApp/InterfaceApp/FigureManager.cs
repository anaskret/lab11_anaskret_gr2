﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceApp
{
    public static class FigureManager
    {
        public static double GetAreas(this IEnumerable<IFigure> collection)
        {
            double sum = 0;
            foreach(var item in collection)
            {
                sum += item.GetArea();
            }
            return sum;
        }
    }
}
